# Travel Itinerary and Plans: Montenegro 28.June - 9.July 2017 #
### Tim, Matthew and Daniél

## Day 1:  Podgorica -> Virpazar Wednesday, 28.June 2017
* Activities: Matthew arrives Podgorica via Belgrade on Serbia flight #172 at 14:35
Daniél arrives Podgorica via Belgrade overnight train.  Matthew and Daniél will either
meet at the Podgorica airport and take a cab together to get to Virpazar or they will 
meet in Virpazar in the late afternoon.  Tim will already be in Virpazar coming
from Albania.
* Lodging: [Country House Djurisic](http://www.booking.com/Share-DbXV1W) (Matthew's Booking

## Day 2: Lake Skadar Thursday, 29.June 2017
* Activities: Bus or taxi from Virpazar? ... hiking around the lake, lots of forest trails, possible dinner at 
[Stari Most](http://www.ethnogastro-balkan.net/eng/p/4624/shkoder%7Cskadar_lake/stari_most_restaurant/about_place)
* Lodging: [Guest House Ljudmila](https://bitbucket.org/bijkler/mytravel/raw/6917c2f69a0c1ac4c64a44ea7aec4250832f94b2/20170508%20BookingCOM%20Guest%20House%20Ljudmila%20Rijeka%20Crnovjevica%20reservatie%2029%20jun-1%20jul%202017.pdf) Rijeka Crnojevica (Tim's Booking)

## Day 3: Lake Skadar Friday, 30.June 2017
* Activities: more hiking around the lake, kayaking?, more forest trails
* Lodging: [Guest House Ljudmila](https://bitbucket.org/bijkler/mytravel/raw/6917c2f69a0c1ac4c64a44ea7aec4250832f94b2/20170508%20BookingCOM%20Guest%20House%20Ljudmila%20Rijeka%20Crnovjevica%20reservatie%2029%20jun-1%20jul%202017.pdf) Rijeka Crnojevica (Tim's Booking)

## Day 4: Kolosin Saturday, 1.July 2017
* Activities: Daniél splits off to go to coast, 
it should be about 1.5 hours from Lake Skadar, Matthew and Tim do day hike to Black Lake
* Lodging: [Apartment Knez](https://bitbucket.org/bijkler/mytravel/raw/33f7b80226fc079749f6b3299f4c5a7e8672ae33/20170508%20BookingCOM%20Apartment%20Knez%20Kolosin%20reservatie%201-3%20jul%202017.pdf), alternative is [Etno Selo Koliba Damjanovica](http://www.booking.com/Share-BMN9RQ) (Tim's bookings)

## Day 5: Biogradska Gora National Park back to Kolosin Sunday, 2.July 2017
* Activities: 10 minute taxi ride into park, big hike in Biogradska Gora 
(see so called "Mountain Eyes")
* Lodging: [Apartment Knez](https://bitbucket.org/bijkler/mytravel/raw/33f7b80226fc079749f6b3299f4c5a7e8672ae33/20170508%20BookingCOM%20Apartment%20Knez%20Kolosin%20reservatie%201-3%20jul%202017.pdf), alternative is [Etno Selo Koliba Damjanovica](http://www.booking.com/Share-BMN9RQ) (Tim's bookings)

## Day 6: Rafting on Tara River Monday, 3.July 2017
* Activities: Tim and Matthew river rafting day [Montenegro Adventures](http://www.montenegro-adventures.com/) 
with transfer at end of trip to Zabljak; try to get a map in Zabljak of Durmitor and discuss hiking trails
* Lodging: [Apartments Peaks](http://www.booking.com/Share-GvGxCR) (Matthew's booking)

## Day 7: Durmitor, Zabljak Tuesday, 4.July 2017
* Activities: Tim and Matthew hiking of our own free will
* Lodging: [Apartments Peaks](http://www.booking.com/Share-GvGxCR) (Matthew's booking)

## Day 8: Durmitor, Zabljak Wednesday, 5.July 2017
* Activities: Tim and Matthew hiking of our own free will
* Lodging: [Apartments Peaks](http://www.booking.com/Share-GvGxCR) (Matthew's booking)

## Day 9: Durmitor to Cetinje Thursday, 6.July 2017
* Activities: Daniél joins Matthew, Tim may split off here or may stay, travel 
from Durmitor to Cetinje, Tim may part ways Daniél and Matthew could do 
short hike in Lovcen National Park
* Lodging: [Apartment Tea](http://www.booking.com/Share-O0ZrhR) (Daniél's booking)

## Day 10: Cetinje Friday, 7.July 2017
* Activities: Daniél and Matthew (and maybe Tim) tour old Montenegro capital
* Lodging: [Apartment Tea](http://www.booking.com/Share-O0ZrhR) (Daniél's booking)

## Day 11: Kotor Saturday 8.July 2017
* Activities: Tim splits off here to return to Albania, Matthew and Daníel swimming and hiking around bay
* Lodging: [Kotor Kelly Apartments](http://www.booking.com/Share-kYZG6J) (Daniél's booking)

## Day 12: Kotor Sunday, 9.July 2017
* Activities: Matthew and Daniél swimming and hiking around bay
* Lodging: [Kotor Kelly Apartments](http://www.booking.com/Share-kYZG6J) (Daniél's booking)

## Day 13: Dubrovnik, Croatia Monday, 10.July 2017
* Activities: Daniél and Matthew take bus from [Kotor to Dubrovnik](https://getbybus.com/en/bus-from-podgorica), hang out
in city
* Lodging: [Old City Inn](http://www.booking.com/Share-euR4QJ) (Matthew's booking)

## Day 14: Dubrovnik, Croatia Tuesday, 11.July 2017
* Activities: Daniél and Matthew hang out in city
* Lodging: [Old City Inn](http://www.booking.com/Share-euR4QJ) (Matthew's booking)

## Day 15: Dubrovnik, Croatia Wednesday, 12.July 2017
* Activities: Matthew departs on Wednesday, 12 July 2017 from Dubrovnik on 
Vueling flight #7724 at 10:15




